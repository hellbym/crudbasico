﻿using System.Data.Entity;

namespace CRUDbasico
{
    public class Persona
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }
    }

    class PersonaContext : DbContext
    {
        public DbSet<Persona> Personas { get; set; }
    }
}
