﻿using System;
using System.Linq;
using static System.Console;

namespace CRUDbasico
{
    static class Program
    {
        static void Main()
        {             
            WriteLine("OPCIONES CRUD:");
            WriteLine("==============\n");
            WriteLine("[1] Crear persona");
            WriteLine("[2] Ver persona");
            WriteLine("[3] Ver todas las personas");
            WriteLine("[4] Ver último registro");
            WriteLine("[5] Actualizar persona");
            WriteLine("[6] Borar persona");
            WriteLine("[7] Salir");
            Write("\nSelección: ");
            var s = ReadLine();

            using(var db = new PersonaContext())
            {
                int id;
                switch(s)
                {
                    case "1":
                    CrearPersona(db);
                    break;

                    case "2":
                    WriteLine("Indique el ID de la persona: ");
                    id = Convert.ToInt16(ReadLine());
                    VerPersona(db, id);
                    break;

                    case "3":
                    VerTodasPersona(db);
                    break;

                    case "4":
                    VerUltimoRegistro(db);
                    break;

                    case "5":
                    Write("Indique el ID de la persona: ");
                    id = Convert.ToInt16(ReadLine());
                    ActualizarPersona(db, id);
                    break;

                    case "6":
                    Write("Indique el ID de la persona: ");
                    id = Convert.ToInt16(ReadLine());
                    BorrarPersona(db, id);
                    break;

                    case "7":
                    Environment.Exit(0);
                    break;
                }
            }

            ReadLine();
        }

        /// <summary>
        /// Agrega un registro de tipo PErsona a la base de datos
        /// </summary>
        /// <param name="db"></param>
        static void CrearPersona(PersonaContext db)
        {
            var persona = new Persona();

            Write("Nombre: ");
            persona.Nombre = ReadLine();

            Write("Apellido: ");
            persona.Apellido = ReadLine();

            Write("Edad: ");
            persona.Edad = Convert.ToInt16(ReadLine());

            db.Personas.Add(persona);

            db.SaveChanges();

            WriteLine("Datos guardados!!!");
        }

        /// <summary>
        /// Presenta en pantalla un registro de tipo Persona con solo agregar su ID
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        static void VerPersona(PersonaContext db, int id)
        {
            var query = from q in db.Personas
                        where q.Id == id
                        select q;

            foreach(var item in query)
            {
                WriteLine("Nombre: " + item.Nombre);
                WriteLine("Apellido: " + item.Apellido);
                WriteLine("Edad: " + item.Edad);
            }

        }

        /// <summary>
        /// Presenta en pantalla todos los registros de tipo Persona que existan en la base de datos
        /// </summary>
        /// <param name="db"></param>
        static void VerTodasPersona(PersonaContext db)
        {
            var query = from q in db.Personas
                        select q;

            foreach(var item in query)
            {
                WriteLine("Id: " + item.Id);
                WriteLine("Nombre: " + item.Nombre);
                WriteLine("Apellido: " + item.Apellido);
                WriteLine("Edad: " + item.Edad);
                WriteLine("\n");
            }

        }

        /// <summary>
        /// Presenta en pantalla el último registro de tipo Persona insertado en la base de datos
        /// </summary>
        /// <param name="db"></param>
        static void VerUltimoRegistro(PersonaContext db)
        {
            var query = (from q in db.Personas
                         orderby q.Id descending
                         select q).First();

            WriteLine("Id: " + query.Id);
            WriteLine("Nombre: " + query.Nombre);
            WriteLine("Apellido: " + query.Apellido);
            WriteLine("Edad: " + query.Edad);
        }

        /// <summary>
        /// Edita todos los campos de un registro de tipo Persona con solo indicar el ID
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        static void ActualizarPersona(PersonaContext db, int id)
        {
            var query = from q in db.Personas
                        where q.Id == id
                        select q;

            foreach(var item in query)
            {
                WriteLine("Nombre actual: " + item.Nombre);
                Write("Nombre nuevo: ");
                item.Nombre = ReadLine();
                WriteLine("\n");

                WriteLine("Apellido actual: " + item.Apellido);
                Write("Apellido nuevo: ");
                item.Apellido = ReadLine();
                WriteLine("\n");

                WriteLine("Edad actual: " + item.Edad);
                Write("Edad nuevo: ");
                item.Edad = Convert.ToInt16(ReadLine());
                WriteLine("\n");
            }

            db.SaveChanges();

            WriteLine("Datos actualizados!!!");
        }

        /// <summary>
        /// Este método elimina un registro con solo agregar su ID
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        static void BorrarPersona(PersonaContext db, int id)
        {
            var query = from q in db.Personas
                        where q.Id == id
                        select q;

            foreach(var item in query)
            {
                db.Personas.Remove(item);
            }

            db.SaveChanges();

            WriteLine("Registro eliminado!!!");
        }
    }
}
